/*console.log = function() {

};
*/

//llama el objecto que ta da acceso a los archivos ya sea de webkit o de firefox
window.requestFileSystem = window.requestFileSystem || window.webkitRequestFileSystem;

//Pide al usuario acceso al sistema de archivos
//por parte de javascript
window.requestFileSystem(window.PERSISTENT, 5 * 1024 * 1024, successFileSystem, errorFileSystem);

var app, fs, loppFunction;

// es llamado
//al terminar de cargar el systema de archivos
function successFileSystem(data) {

    console.log("Success:", data);
    window.webkitStorageInfo.requestQuota(window.PERSISTENT, 1024 * 1024,
        function(grantedBytes) {
            window.requestFileSystem(window.PERSISTENT, grantedBytes, finishGranting, errorFileSystem);
        },
        function(e) {
            console.log("Error", e);
        });

}

//Es llamado al terminar de brindar
//accesso al sistema de archivos
function finishGranting(data) {
    console.log("Success:", data);
    fs = data;
    loppFunction = setTimeout(readFile, 1000);
}

//Es llamado al ocurrir un error al 
//pedir al usuario el acceso a los 
//archivos
function errorFileSystem(data) {
    console.log("Error:", data);
}

//Guarda el archivo (-_-)
function saveFile(text) {
    fs.root.getFile("log.txt", { create: true }, function(fileEntry) {

            fileEntry.createWriter(function(fileWriter) {
                fileWriter.onwriteend = function() {
                    console.log("finished Writing");
                };

                fileWriter.onerror = function(e) {
                    console.log("Error:" + e.toString());
                };

                var blobBuild = new Blob([JSON.stringify(text)], { type: "text/plain" });

                fileWriter.write(blobBuild);
            });

        },
        function(data) {

            console.log("Error:", data);

        });
}

// lee el archvivo con la informacion
// del ultimo examen e intenta registrarlo
// ... -_-
function readFile() {
    console.log("----called readFile:----- \n");

    fs.root.getFile("log.txt", {}, function(data) {

        data.file(function(file) {

                var reader = new FileReader();
                // console.log(reader);

                reader.onloadend = function() {

                    console.log(this.result);

                    var obj = JSON.parse(this.result);

                    if (angular.element(document.body).scope().procesarUltimoExamen) {

                        console.log("Entered File Registration");

                        angular.element(document.body).scope().procesarUltimoExamen(obj);

                    }

                    return obj;
                };
                reader.onprogress = function() {
                    // console.log("Progressing over here man:", p);
                };
                reader.onerror = function(e) {
                    console.log("Giving errors over hean meaaaan", e);
                };

                reader.readAsText(file);

            }, function(error) { console.log("error:", error); }
        );
    }, function() { /*console.log("error:", error); */ });
    setTimeout(readFile, 1000);
}

//duerme el hilo ..... (-_-)
function sleep(milliseconds) {
    var start = new Date().getTime();
    for (var i = 0; i < 1e7; i++) {
        if ((new Date().getTime() - start) > milliseconds) {
            break;
        }
    }
}

//borra el archivo .... (-_-)
function deleteFile() {
    fs.root.getFile("log.txt", { create: false }, function(data) {
        data.remove(function() {
            console.log("File Removed");
        }, function() {
            console.log("Error removing the file");
        }, function(data2) { console.log("error:", data2); });
    }, function(data) { console.log("error:", data); });
}